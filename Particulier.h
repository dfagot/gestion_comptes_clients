#ifndef PARTICULIER_H
#define PARTICULIER_H

#include "Client.h"


class Particulier : public Client
{
    public:
        Particulier(string, string, char, int, map<string, string>, vector<Compte*>, char, int);
        ~Particulier();

        char getsituation_familiale() { return situation_familiale; }
        void setsituation_familiale(char val) { situation_familiale = val; }
        int getdate_naissance() { return date_naissance; }
        void setdate_naissance(int val) { date_naissance = val; }

    protected:

    private:
        char situation_familiale;
        int date_naissance;
};

#endif // PARTICULIER_H
