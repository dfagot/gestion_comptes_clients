#include "Compte.h"

Compte::Compte(int n, int d, float s, float decouv)
{
    //ctor
    setnumero(n);
    setdate_ouverture(d);
    setsolde(s);
    setdecouvert_autorise(decouv);
}

Compte::~Compte()
{
    //dtor
}

void Compte::ajouter_operation(Operation* pop)
{
    liste_operations.push_back(pop);
    cout << "operation ajoutee avec succes" << endl;
}

void Compte::lister_operations()
{
    cout << "Liste des operations pour le compte #" << getnumero() << " -----------" << endl;

    int i = 0;

    for (auto pop:liste_operations)
    {
        cout << "operation #" << i << " : " <<  pop->to_string() << endl;
        i++;
    }

    cout << "----------------------------------------------------" << endl;
}

void Compte::modifier_operation(int iop, int date, int code, float montant)
{
    Operation* po = getliste_operations()[iop];
    po->setdate(date);
    po->setcode(code);
    po->setmontant(montant);
}
