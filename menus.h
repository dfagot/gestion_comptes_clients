#ifndef MENUS_H_INCLUDED
#define MENUS_H_INCLUDED
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
//#include <boost/algorithm/string.hpp>
using namespace std;

void clear_console()
{
#if defined _WIN32
    system("cls");
#elif defined (__LINUX__) || defined(__gnu_linux__) || defined(__linux__)
    system("clear");
#elif defined (__APPLE__)
    system("clear");
#endif
}

bool short_string_checker(string);
bool date_checker(int);

void menu_principal(Banque&); // affiche le menu principal
void menu_visu(Banque&); // affiche le menu de visu
void menu_clients(Banque&); // affiche le menu de modifs sur clients
void menu_ops(Banque&); // affiche le menu de visu/modifs sur operations

void menu_principal(Banque& b)
{
    cout << "[MENU PRINCIPAL] Que souhaiteriez-vous faire?" << endl << " 1 - Consulter" << endl << " 2 - Modifs clients (" << b.getliste_clients().size() << " enregistres)" << endl << " 3 - Modifs ops";
    cout << endl << " 0 - Quitter" << endl;

    char c = '7';
    cin >> c;

    switch (c)
    {
        case '1':
            cout << "Menu visualisation" << endl;
            menu_visu(b);
            break;

        case '2':
            cout << "Menu clients" << endl;
            menu_clients(b);
            break;
            //menu_principal(b);//break;

        case '3':
            cout << "Menu ops" << endl;
            menu_ops(b);
            break;
            //menu_principal(b);//break;

        case '0':
            cout << "Sortie du programme" << endl;
            break;

        default:
            cout << "Choix incorrect" << endl;
            //menu_principal(b);

    }

    if (c != '0') menu_principal(b);


}

void consulter_compte(Banque& b)
{
    int n;
    cout << "Quel compte souhaitez-vous consulter?";
    cin >> n;

    for(auto pc:b.getliste_clients())
    {
        for (auto pco:pc->getliste_comptes())
        {
            if (pco->getnumero() == n)
            {
                cout << "Informations du compte #" << n << " (client : " << pc->getprenom() << " " << pc->getnom() << ")" << endl;
                cout << "Solde = " << pco->getsolde() << " euros, decouvert autorise = " << pco->getdecouvert_autorise() << " euros" << endl;
                return;

            }
        }
    }
    cout << "Compte introuvable" << endl;
}

void consulter_ops(Banque &b)
{
    int n;
    cout << "Voir les operations de quel compte? ";
    cin >> n;

    for (auto pc:b.getliste_clients())
    {
        for (auto pco:pc->getliste_comptes())
        {
            if (pco->getnumero() == n)
            {
                pco->lister_operations();
            }
        }
    }
}

void menu_visu(Banque& b)
{
    //menu_principal();
    cout << "[MENU VISUALISATION] Que souhaiteriez-vous faire?" << endl << " 1 - Lister clients" << endl << " 2 - Consulter compte" << endl << " 3 - Consulter ops d'un compte" << endl << " 0 - Retour" << endl;

    char c = '7';
    cin >> c;

    switch (c)
    {
        case '1':
            //cout << "Menu visualisation" << endl;
            b.lister_clients();
            break;

        case '2':
            consulter_compte(b);
            break;

        case '3':
            consulter_ops(b);
            break;

        case '0':
            //menu_principal(b);
            break;

        default:
            cout << "Choix incorrect" << endl;
            //menu_visu(b);
    }

    if (c != '0' ) menu_visu(b);
    clear_console();
}


bool short_string_checker(string input)
{

    int l = input.size();
    if (l<=50)
    {
        return true;
    }
    else
    {
        cout << "chaine trop longue (>50 caracteres)" << endl;
        return false;
    }
}

bool date_checker(int input)
{
    int annee = 0, mois = 0, jour = 0;

    annee = input/10000;
    mois = (input - 10000*annee)/100;
    jour = input - 10000*annee - 100*mois;

    if (jour<=31 && mois<=12) return true;
    else
    {
        cout << "erreur dans la date" << endl;
        return false;
    }

}

bool tel_checker(int tel)
{
    string tel_str = to_string(tel);

    if (tel_str.size() == 10) return true;
    else
    {
        cout << "erreur de format de numero de telephone" << endl;
        return false;
    }
}

void ajout_compte(Client* pc)
{
    // Attributs comptes
    int nc, dc;
    float so, da;

    cout << "Saisissez un numero de compte : ";
    cin >> nc;
    cout << "Saisissez une date de creation : ";
    cin >> dc;
    cout << "Saisissez un solde : ";
    cin >> so;
    cout << "Saisissez le decouvert autorise : ";
    cin >> da;

    Compte* pco = new Compte(nc, dc, so, da); // verifier doublon nc
    pc->ajouter_compte(pco);

    cout << "Compte " << nc << " cree avec succes" << endl;
}

void ajout_client(Banque& b)
{
    // Attributs Clients
    string nom, prenom;
    int date_naissance, tel;
    char c, s, sf;
    map<string, string> adresse;

    // Attributs pros
    string rs, am;
    int siret, date_creation;
    map<string, string> adresse_siege;

    vector<Compte*> comptes;

    // Creation Client
    cout << "Rentrez les informations du nouveau client:" << endl;
    cout << "Est-ce un particulier(1) ou un professionnel (2)? ";
    cin >> c;

    cout << "Rentrez le nom : " ;
    cin >> nom; // cout << "Nom? ";
    cout << "Rentrez le prenom : ";
    cin >> prenom;
    cout << "Rentrez le sexe : ";
    cin >> s;
    cout << "Rentrez le telephone : ";
    cin >> tel;
    cout << "Rentrez l'adresse (libelle) : ";
    cin.ignore();
    getline(cin, adresse["libelle"]);//cin >> adresse["libelle"];
    cout << "Rentrez l'adresse (complement) : ";
    cin.ignore();
    getline(cin, adresse["complement"]);//cin >> adresse["complement"];
    cout << "Rentrez l'adresse (code postal) : ";
    cin >> adresse["code_postal"];
    cout << "Rentrez l'adresse (ville) : ";
    cin >> adresse["ville"];

    Client* pc;

    // Adapter infos suivant le statut du Client
    if (c == '1')
    {
        cout << "Rentrez votre statut familial : ";
        cin >> sf;
        // checker ...
        cout << "Rentrez votre date de naissance : ";
        cin >> date_naissance;

        pc = new Particulier(nom, prenom, s, tel, adresse, comptes, sf, date_naissance);
    }

    else
    {
        cout << "Rentrez le numero siret : " ;
        cin >> siret;
        cout << "Rentrez la raison sociale : " ;
        cin >> rs;
        cout << "Rentrez l'annee de creation : ";
        cin >> date_creation;
        cout << "Rentrez l'adresse du siege (libelle) : ";
        cin.ignore();
        getline(cin, adresse_siege["libelle"]);
        cout << "Rentrez l'adresse du siege (complement) : ";
        cin.ignore();
        getline(cin, adresse_siege["complement"]);
        cout << "Rentrez l'adresse du siege (code postal) : ";
        cin >> adresse_siege["code_postal"];
        cout << "Rentrez l'adresse du siege (ville) : ";
        cin >> adresse_siege["ville"];
        cout << "Rentrez l'adresse mail : " ;
        cin >> am;

        pc = new Professionnel(nom, prenom, s, tel, adresse, comptes, siret, rs, date_creation, adresse_siege, am);
    }

    b.ajouter_client(pc);
}

void modification_client(Banque& b)
{
    // Attributs Clients
    string nom, prenom;

    cout << "Quel client souhaitez-vous modifier?" << endl;
    cout << "Nom : ";
    cin >> nom;

    int n = 0;
    for (auto pc:b.getliste_clients())
    {
        if (pc->getnom() == nom)
        {
            prenom = pc->getprenom();
            n++;
        }
    }

    cout << n << " clients trouves" << endl;

    if (n == 0) cout << "client introuvable" << endl;
    else
    {
        char c;
        // hypothese : n == 1

        if (n > 1)
        {
            cout << "Plusieurs occurence de ce nom. Precisez le prenom : ";
            cin >> prenom;
        }

        // Menu choix atribut a modifier, a afficher en fonction du type de Client
        cout << "Que souhaitez-vous modifier?" << endl << " 1 - num tel" << endl << " 2 - Ajout de compte" << endl;
        cin >> c;

        // switch sur modifications possibles



        for (auto pc:b.getliste_clients())
        {
            if (pc->getnom() == nom && pc->getprenom() == prenom)
            {

                switch (c)
                {
                    case '1':
                        int tel;
                        cout << "Saisissez le nouveau numero : ";
                        cin >> tel;
                        pc->settelephone(tel);
                        cout << "Numero de telephone mis à jour" << endl;
                        break;

                    case '2':
                        ajout_compte(pc);
                        break;
                    default:
                        cout << "choix inconnu";
                }


            }

        }

    }
}

void menu_clients(Banque& b)
{
    cout << "[MENU CLIENTS] Que souhaiteriez-vous faire? (" << b.getliste_clients().size() << " enregistres)" << endl << " 1 - Ajouter client" << endl << " 2 - Modifier client" << endl;
    cout << " 3 - Supprimer client" << endl << " 0 - Retour" << endl;

    char c = '7';
    cin >> c;

    switch (c)
    {
        case '1':
            ajout_client(b);
            break;

        case '2':
            modification_client(b);
            break;

        case '3':
            b.lister_clients();
            int id;
            cout << "Quel client souhaitez-vous supprimer?" << endl;
            cin >> id;
            //cout << "Supression client #" << id << "(" << b.getliste_clients()[id]->getprenom() << " " << b.getliste_clients()[id]->getnom() << ")" << endl;
            b.supprimer_client(id);
            cout << "Client " << id << " supprime" << endl;
            break;

        case '0':
            // retour au menu principal
            break;

        default:
            cout << "Choix incorrect" << endl;
            //menu_clients(b);
    }

    if (c != '0' ) menu_clients(b);
    cout << endl;
    clear_console();

}

void ajout_op(Banque& b)
{
    int n;
    cout << "A quel compte souhaitez-vous ajouter une operation? ";
    cin >> n;

    for (auto pc:b.getliste_clients())
    {
        for (auto pco:pc->getliste_comptes())
        {
            if (pco->getnumero() == n)
            {

                int date, code;
                float montant;

                cout << "Rentrez la date d'operation : ";
                cin >> date;
                cout << "Rentrez le code d'operation (1|2|3) : ";
                cin >> code;
                cout << "Rentrez le montant : ";
                cin >> montant;

                // ajouter l'op
                Operation* po = new Operation(n, date, code, montant);
                pco->ajouter_operation(po);
            }
        }
    }
}

void modif_op(Banque& b)
{
    int n;
    cout << "Sur quel compte souhaitez-vous modifier une operation? ";
    cin >> n;

    for (auto pc:b.getliste_clients())
    {
        for (auto pco:pc->getliste_comptes())
        {
            if (pco->getnumero() == n)
            {

                pco->lister_operations();

                int iop;
                cout << "Quelle operation? ";
                cin >> iop;

                int date, code;
                float montant;

                cout << "Rentrez la date d'operation : ";
                cin >> date;
                cout << "Rentrez le code d'operation (1|2|3) : ";
                cin >> code;
                cout << "Rentrez le montant : ";
                cin >> montant;

                // ajouter l'op
                pco->modifier_operation(iop, date, code, montant);
            }
        }
    }
}

void erase_op(Banque& b)
{
    int n;
    cout << "Sur quel compte souhaitez-vous supprimer une operation? ";
    cin >> n;

    for (auto pc:b.getliste_clients())
    {
        for (auto pco:pc->getliste_comptes())
        {
            if (pco->getnumero() == n)
            {

                pco->lister_operations();

                int iop;
                cout << "Quelle operation? ";
                cin >> iop;

                // ajouter l'op
                pco->effacer_operation(iop);
            }
        }
    }
}

void importer_ops()
{
    string operation;
    string num, date, code, montant;
    string delimiter=";";

    ifstream input("./io/Operations.txt");

    while (input >> operation)
    {
        //cout << "Chaine lue : " << operation << endl;
        num = operation.substr(0, operation.find(delimiter));
        date = operation.substr(sizeof(num), operation.find(delimiter));
        code = operation.substr(sizeof(date) + sizeof(num), operation.find(delimiter));
        montant = operation.substr(sizeof(code) + sizeof(date) + sizeof(num), operation.find("\n"));

        //cout << num << date << code << montant << endl;

    }
    input.close();
}

void export_ops(Banque& b)
{
    ofstream output("./io/export.txt");
    ofstream anomalies("./io/Anomalies.txt")

    output << "Operations de la semaine : " << endl;
    anomalies << "Anomalies detectees : " << endl;

    for (auto pc:b.getliste_clients())
    {
        for (auto pco:pc->getliste_comptes())
        {
            for (auto pop:pco->getliste_operations())
            {
                if(pop->getcode() == 1 || pop->getcode() == 2 || pop->getcode() == 3 ) output << pop->to_string();
                else anomalies << pop->to_string();
            }
        }
    }
    output.close();
    anomalies.close();
}

void menu_ops(Banque& b)
{
    cout << "[MENU OPERATIONS] Que souhaiteriez-vous faire?" << endl << " 1 - Ajouter operation" << endl << " 2 - Modifier operation" << endl;
    cout << " 3 - Supprimer operation" << endl << " 4 - Export d'operations" << endl << " 5 - Import d'operations" << endl << " 0 - Retour" << endl;

    char c = '7';
    cin >> c;

    switch (c)
    {
        case '1':
            //cout << "Ajout op indisponible" << endl;
            ajout_op(b);
            break;

        case '2':
            //cout << "Modification op indisponible" << endl;
            modif_op(b);
            break;

        case '3':
            //cout << "Supprimer op indisponible" << endl;
            erase_op(b);
            break;

        case '4':
            cout << "Export des operations en cours..." << endl;
            export_ops(b);
            break;

        case '5':
            //cout << "Import des operations en cours..." << endl;
            importer_ops();
            //menu_visu(b);
            break;

        case '0':
            //menu_principal(b);
            break;

        default:
            cout << "Choix incorrect" << endl;
    }

    if (c != '0') menu_ops(b);
    cout << endl;
    clear_console();
}

#endif // MENUS_H_INCLUDED
