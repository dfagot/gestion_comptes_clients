#ifndef PROFESSIONNEL_H
#define PROFESSIONNEL_H

#include "Client.h"


class Professionnel : public Client
{
    public:
        Professionnel(string, string, char, int, map<string, string>, vector<Compte*>, int, string, int, map<string, string>, string);
        ~Professionnel();

        int getsiret() { return siret; }
        void setsiret(int val) { siret = val; }
        string getraison_sociale() { return raison_sociale; }
        void setraison_sociale(string val) { raison_sociale = val; }
        int getannee_creation() { return annee_creation; }
        void setannee_creation(int val) { annee_creation = val; }
        map<string, string> getadresse_entreprise() { return adresse_entreprise; }
        void setadresse_entreprise(map<string, string>);
        string getadresse_mail() { return adresse_mail; }
        void setadresse_mail(string val) { adresse_mail = val; }

    protected:

    private:
        int siret;
        string raison_sociale;
        int annee_creation;
        map<string, string> adresse_entreprise;
        string adresse_mail;
};

#endif // PROFESSIONNEL_H
