#include "Professionnel.h"

Professionnel::~Professionnel()
{
    //dtor
}

Professionnel::Professionnel(string n, string p, char se, int t, map<string, string> ad, vector<Compte*> c, int s, string rs, int ac, map<string, string> ad_siege, string am)
    : Client::Client(n, p, se, t, ad_siege, c)
{
    setsiret(s);
    setraison_sociale(rs);
    setannee_creation(ac);
    setadresse_entreprise(ad_siege);
    setadresse_mail(am);
}

void Professionnel::setadresse_entreprise(map<string, string> new_adresse)
{

    adresse_entreprise = new_adresse;
}
