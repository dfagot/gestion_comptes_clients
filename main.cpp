#include "Particulier.h"
#include "Professionnel.h"
#include "Banque.h"
#include "menus.h"

int main()
{

    Banque b;

    // Ajout de clients/comptes initiaux
    Compte* c1 = new Compte(123, 20200601, 5000, 10);
    Operation* pop1 = new Operation(123, 20200623, 2, 500);
    c1->ajouter_operation(pop1);

    Compte* c2 = new Compte(456, 20200602, 500, 1);
    Compte* c3 = new Compte(789, 20200603, 50, 0);
    Compte* c4 = new Compte(987, 20200604, 5, 0.1);

    vector<Compte*> comptes1 = {c1, c2, c3};
    vector<Compte*> comptes2 = {c4};

    map<string, string> ad1;
    ad1["libelle"] = "56 rue ra";
    ad1["complement"] = "apt 15";
    ad1["code_postal"] = "31400";
    ad1["ville"] = "toulouse";

    map<string, string> ad2;
    ad2["libelle"] = "57 rue ra";
    ad2["complement"] = "apt 4532";
    ad2["code_postal"] = "31400";
    ad2["ville"] = "toulouse";

    Client* client1 = new Particulier("Fagot", "Dylan", 'M', 0667377116, ad1, comptes1, 'C', 19930727);
    Client* client2 = new Professionnel("Fagot(pro)", "Dylan", 'M', 0667377116, ad1, comptes2, 123456, "SA", 1993, ad2, "dylan.fagot@akka.eu");

    b.ajouter_client(client1);
    b.ajouter_client(client2);


    // Lancement console
    cout << "--------Bienvenue sur l'outil de gestion de comptes de la banque---------" << endl;
    menu_principal(b);

    return 0;
}
