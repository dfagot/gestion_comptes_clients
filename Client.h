#ifndef CLIENT_H
#define CLIENT_H
#include <iostream>
#include <vector>
#include <map>
using namespace std;

#include "Compte.h"


class Client
{
    public:
        Client(string, string, char, int, map<string, string>, vector<Compte*>);
        virtual ~Client();

        string getnom();
        void setnom(string);
        string getprenom();
        void setprenom(string);
        char getsexe() { return sexe; }
        void setsexe(char);
        int gettelephone() { return telephone; }
        void settelephone(int);
        map<string, string> getadresse();
        void setadresse(map<string, string>);
        vector<Compte*> getliste_comptes() { return liste_comptes; }
        void setliste_comptes(vector<Compte*> val) { liste_comptes = val; }
        void ajouter_compte(Compte* pco){liste_comptes.push_back(pco);};

    protected:

    private:
        string nom; // 50 caracteres max, affichage en caps
        string prenom; // 50 caracteres max, affichage cap + mins
        char sexe; // 'F' ou 'M'
        int telephone; // 10 chiffres
        map<string, string> adresse; // affichage ville en caps
        vector<Compte*> liste_comptes;
};

#endif // CLIENT_H
