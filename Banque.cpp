#include "Banque.h"

Banque::Banque()
{
    //ctor
}

Banque::~Banque()
{
    //dtor
}

void Banque::lister_clients()
{
    cout << "Liste des clients : " << endl;
    cout << "----------------------------------" << endl;

    int i = 0;

    for (auto pc:liste_clients) // Client
    {
        cout << "- Client #" << i << " : " << pc->getprenom() << " " << pc->getnom() << endl;

        for (auto pco:pc->getliste_comptes())
        {
            cout << "Compte #" <<  pco->getnumero() << endl;
        }

        i++;
        cout << "----------------------------------" << endl;
    }
}

void Banque::consulter_comptes_client(int id)
{
    cout << "Soldes des comptes du client " << id << " : " << endl;

    for (auto compte:liste_clients[id]->getliste_comptes())
    {
        cout << "Compte #" << compte->getnumero() << ", solde = " << compte->getsolde() << endl;
    }
}

void Banque::consulter_operations_compte(int n) // lire fichier clients
{
    for (auto client:liste_clients)
    {
        for (auto compte:client->getliste_comptes())
        {
            if (compte->getnumero() == n) cout << "compte trouve " << endl; //compte->consulter_operations();
            break;
        }
    }
    // exception
}

void Banque::importer_fichier_operations(string)
{

}

void Banque::ajouter_client(Client* c)
{
    liste_clients.push_back(c);
}

void Banque::modifier_client(int id)
{

}

void Banque::supprimer_client(int id)
{
    liste_clients.erase(liste_clients.begin()+id);
}

void Banque::export_operations_jour() // a date fixee, lister les operations
{

}
