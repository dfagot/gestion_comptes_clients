#ifndef OPERATION_H
#define OPERATION_H
#include <iostream>
#include <sstream>
using namespace std;

class Operation
{
    public:
        Operation(int, int, int, float);
        virtual ~Operation();

        int getdate() { return date; }
        void setdate(int val) { date = val; }
        int getnumero_compte() { return numero_compte; }
        void setnumero_compte(int val) { numero_compte = val; }
        int getcode() { return code; }
        void setcode(int val) { code = val; }
        float getmontant() { return montant; }
        void setmontant(float val) { montant = val; }
        string to_string();

    protected:

    private:
        int date;
        int numero_compte;
        int code;
        float montant;
};

#endif // OPERATION_H
