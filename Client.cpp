#include "Client.h"

Client::~Client()
{
    //dtor
}

Client::Client(string n, string p, char s, int t, map<string, string> ad, vector<Compte*> c)
{
  // constructeur a reutiliser avec constructeurs Particulier/Professionnel
  setnom(n);
  setprenom(p);
  setsexe(s);
  settelephone(t);
  setadresse(ad);
  setliste_comptes(c);
}

void Client::setnom(string n)
{
    /*
    if (n.size()>50) throw;
    else nom = n;
    */
    nom = n;
}

void Client::setprenom(string p)
{
    /*
    if (p.size()>50) throw;
    else prenom = p;
    */
    prenom = p;
}

void Client::setsexe(char s)
{
    /*
    if (toupper(s)=='M' || toupper(s)=='F') sexe = toupper(s);
    else throw;
    */
    sexe = toupper(s);
}

void Client::settelephone(int t)
{
    /*
    if (is_numeric(t) && t.size()==10) telephone = t; // ou via regex
    else throw;
    */
    telephone = t;
}

map<string, string> Client::getadresse()
{
    return adresse;
}

void Client::setadresse(map<string, string> new_adresse)
{

    adresse = new_adresse;
}

string Client::getnom()
{
    return nom;//str_toupper(nom);
}

string Client::getprenom()
{
    return prenom;//str_tosurname(prenom); // via transform
}
