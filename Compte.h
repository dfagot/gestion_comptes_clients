#ifndef COMPTE_H
#define COMPTE_H
#include "Operation.h"
#include <vector>


class Compte
{
    public:
        Compte(int, int, float, float);
        ~Compte();

        int getnumero() { return numero; }
        void setnumero(int val) { numero = val; }
        int getdate_ouverture() { return date_ouverture; }
        void setdate_ouverture(int val) { date_ouverture = val; }
        float getsolde() { return solde; }
        void setsolde(float val) { solde = val; }
        float getdecouvert_autorise() { return decouvert_autorise; }
        void setdecouvert_autorise(float val) { decouvert_autorise = val; }
        void ajouter_operation(Operation*);
        void modifier_operation(int, int, int, float);
        void effacer_operation(int id){liste_operations.erase(liste_operations.begin()+id);}
        void lister_operations();
        vector<Operation*> getliste_operations(){return liste_operations;}

    protected:

    private:
        int numero;
        int date_ouverture;
        float solde;
        float decouvert_autorise;
        vector<Operation*> liste_operations;
};

#endif // COMPTE_H
