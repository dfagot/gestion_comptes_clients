#ifndef BANQUE_H
#define BANQUE_H
#include <string>
#include "Particulier.h"
#include "Professionnel.h"

class Banque
{
    public:
        Banque();
        ~Banque();

        void lister_clients();
        void consulter_comptes_client(int);
        void consulter_operations_compte(int);
        void importer_fichier_operations(string);

        void ajouter_client(Client*);
        void modifier_client(int);
        void supprimer_client(int);

        void export_operations_jour();
        vector<Client*> getliste_clients(){return liste_clients;};

    protected:

    private:
        vector<Client*> liste_clients;
};

#endif // BANQUE_H
