#include "Operation.h"

Operation::Operation(int n, int date, int code, float montant)
{
    //ctor
    setnumero_compte(n);
    setdate(date);
    setcode(code);
    setmontant(montant);
}

Operation::~Operation()
{
    //dtor
}

string Operation::to_string()
{
    ostringstream oss;
    oss << numero_compte << ";" << date << ";" << code << ";" << montant << endl;
    return oss.str();
}
